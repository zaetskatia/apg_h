package com.plyaka.eza.apg_2_h;

/**
 * Created by eza on 17.02.18.
 */

    public class FreqModel
{
    byte Hi1;
    byte Lo1;
    byte Hi2;
    byte Lo2;
    byte Hi3;
    byte Lo3;
    String name;

    public FreqModel(int Hi1, int Lo1, int Hi2, int Lo2, int Hi3, int Lo3, String name) {
        this.Hi1 = (byte) Hi1;
        this.Lo1 = (byte) Lo1;
        this.Hi2 = (byte) Hi2;
        this.Lo2 = (byte) Lo2;
        this.Hi3 = (byte) Hi3;
        this.Lo3 = (byte) Lo3;
        this.name = name;
    }
}
