package com.plyaka.eza.apg_2_h;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements IProgramButtonClickListener
{
    ImageView batteryImageView;
    View signalImageView;
    LinearLayout signalLineContainer;

    MainFragment mfrag;
    ProgramFragment pfrag;

    public String recievedString;
    public String recievedStringRecover = "";
    private Bluetooth btInterface;
    EProgram currentProgram;
    boolean isRecoverConnection = false;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setTitle("Medbiotech Zoo");

        initElements();

        btInterface = new Bluetooth();

        if (Bluetooth.blue_ENABLE == 1)
        {
            startActivityForResult(btInterface.enableBtIntent, 1);
            if(Bluetooth.status == 10)
            {
                Toast.makeText(getBaseContext(), "...Connection is NOT ESTABLISHED - try to restart the application ", Toast.LENGTH_SHORT).show();

            }
        }


      //  setButteryLevel(439);
       // setSignalLevel(592);

        pfrag = new ProgramFragment();

        mfrag = new MainFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frgmCont, mfrag);
        ft.commit();

        Bluetooth.writeData((byte) 53);

        testConnection();

    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == 0)
//        {
//            Toast.makeText(getBaseContext(), "Need to turn on Bluetooth!", Toast.LENGTH_SHORT).show();
//        }
//        if (requestCode == 1)
//        {
//            btInterface = new Bluetooth();
//            if(Bluetooth.status == 10)
//            {
//                Toast.makeText(getBaseContext(), "...Connection is NOT ESTABLISHED - try to restart the application ", Toast.LENGTH_SHORT).show();
//
//            }
//        }
//    }

    private void initElements()
    {
        batteryImageView = (ImageView) findViewById(R.id.batteryImageView);
        signalImageView = findViewById(R.id.signalLineView);
        signalLineContainer = (LinearLayout) findViewById(R.id.signalLineContainer);
    }


    private  void testConnection()
    {
        Bluetooth.hBlue = new Handler() {
            @Override
            public void handleMessage(android.os.Message msg) {
                switch (msg.what) {
                    case 1: {
                        byte[] readBuf = (byte[]) msg.obj;

                        if(readBuf.length == 4 )
                        {
                           // Toast.makeText(getBaseContext(), "recieved: " + readBuf[0]+ " "+ readBuf[1]+ " "+ readBuf[2]+ " " + readBuf[3], Toast.LENGTH_SHORT).show();
                            TimerController.getInstance().stopTimer();
                            setSignalLevel(readBuf[0] * 256 + Extentions.ToUnsigned(readBuf[1]), readBuf[2] * 256 + Extentions.ToUnsigned(readBuf[3]));
                        }

                        if(readBuf.length == 3)
                        {
                         //   Toast.makeText(getBaseContext(), "recieved: " + readBuf[0]+ " "+ readBuf[1]+ " "+ readBuf[2], Toast.LENGTH_SHORT).show();
                          //  setSignalLevel(readBuf[0] * 256 + Extentions.ToUnsigned(readBuf[1]), readBuf[2] * 256 + Extentions.ToUnsigned(readBuf[3]));
                        }

                        recievedString = Math.abs(readBuf[0]) + "";
                        if(readBuf.length == 2)
                        {
                            recievedStringRecover = Math.abs(readBuf[1]) + "";
                        }

                        if (recievedString.equals("25"))
                        {
                            //Toast.makeText(getBaseContext(), "recieved: " + recievedString, Toast.LENGTH_SHORT).show();
                            GoToMainFragment();

                            return;
                        }

                        if (recievedString.equals("49"))
                        {
                           // Toast.makeText(getBaseContext(), "recieved: " + recievedString, Toast.LENGTH_SHORT).show();
                            TimerController.getInstance().startTimer();

                            return;
                        }


                        if (recievedString.equals("31"))
                        {
                          //  Toast.makeText(getBaseContext(), "recieved: " + recievedString, Toast.LENGTH_SHORT).show();
                            isRecoverConnection = true;
                        }

                        if (recievedString.equals("11") || recievedStringRecover.equals("11"))
                        {
                            FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.replace(R.id.frgmCont, pfrag);
                            ft.commit();

                          //  Toast.makeText(getBaseContext(), "recieved: " + recievedString, Toast.LENGTH_SHORT).show();
                           // pfrag.drawCircle(R.mipmap.circle);
                            return;
                        }

                        if (recievedString.equals("12")|| recievedStringRecover.equals("12"))
                        {
                           // Toast.makeText(getBaseContext(), "recieved: " + recievedString, Toast.LENGTH_SHORT).show();

                            if(isRecoverConnection)
                            {
                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.replace(R.id.frgmCont, pfrag);
                                ft.commit();
                                isRecoverConnection = false;
                                return;
                            }

                            pfrag.drawCircle(R.mipmap.circle1);
                            return;
                        }
                        if (recievedString.equals("13")|| recievedStringRecover.equals("13"))
                        {
                          //  Toast.makeText(getBaseContext(), "recieved: " + recievedString, Toast.LENGTH_SHORT).show();

                            if(isRecoverConnection)
                            {
                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.replace(R.id.frgmCont, pfrag);
                                ft.commit();
                                isRecoverConnection = false;
                                return;
                            }

                            pfrag.drawCircle(R.mipmap.circle2);
                            return;
                        }
                        if (recievedString.equals("14")|| recievedStringRecover.equals("14"))
                        {
                          //  Toast.makeText(getBaseContext(), "recieved: " + recievedString, Toast.LENGTH_SHORT).show();

                            if(isRecoverConnection)
                            {
                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.replace(R.id.frgmCont, pfrag);
                                ft.commit();
                                isRecoverConnection = false;
                                return;
                            }

                            pfrag.drawCircle(R.mipmap.circle3);
                            return;
                        }
                        if (recievedString.equals("15")|| recievedStringRecover.equals("15"))
                        {
                          //  Toast.makeText(getBaseContext(), "recieved: " + recievedString, Toast.LENGTH_SHORT).show();

                            if(isRecoverConnection)
                            {
                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.replace(R.id.frgmCont, pfrag);
                                ft.commit();
                                isRecoverConnection = false;
                                return;
                            }

                            pfrag.drawCircle(R.mipmap.circle4);
                            return;
                        }

                    }
                }
            }
        };
    }

    void sendData(final byte[] byteArray)
    {
        TimerController.getInstance().stopTimer();

        Bluetooth.writeData((byte)2);

        for (int i = 0; i < byteArray.length; i++)
        {
            Bluetooth.writeData(byteArray[i]);
        }

        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Bluetooth.writeData((byte)3);
    }


    @Override
    public void onBackPressed()
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Stop");
        alertDialog.setMessage("Are you sure to stop program?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog,int which)
            {
                customBack();

            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    void customBack()
    {
        TimerController.getInstance().stopTimer();
        Bluetooth.writeData((byte) 5);
        super.onBackPressed();
        finish();
    }

    @Override
    public void programButtonClick(EProgram program)
    {
        switch(program)
        {
            case PROGRAM1:
                sendData(FreqModelHolder.fraqArray1);
                currentProgram = program;
                break;
            case PROGRAM2:
                sendData(FreqModelHolder.fraqArray2);
                currentProgram = program;
                break;
            case PROGRAM3:
                sendData(FreqModelHolder.fraqArray3);
                currentProgram = program;
                break;
            case PROGRAM4:
                sendData(FreqModelHolder.fraqArray4);
                currentProgram = program;
                break;
            case PROGRAM5:
                sendData(FreqModelHolder.fraqArray5);
                currentProgram = program;
                break;
            case PROGRAM6:
                sendData(FreqModelHolder.fraqArray6);
                currentProgram = program;
                break;
            case PROGRAM7:
                sendData(FreqModelHolder.fraqArray7);
                currentProgram = program;
                break;
            case PROGRAM8:
                sendData(FreqModelHolder.fraqArray8);
                currentProgram = program;
                break;
            case PROGRAM9:
                sendData(FreqModelHolder.fraqArray9);
                currentProgram = program;
                break;
            case PROGRAM10:
                sendData(FreqModelHolder.fraqArray10);
                currentProgram = program;
                break;
        }
    }

    @Override
    public void GoToMainFragment()
    {
        Fragment frag2 = new MainFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frgmCont, frag2);
        ft.commit();

        testConnection();
        TimerController.getInstance().startTimer();
    }

    @Override
    public void setProgName()
    {
        if(currentProgram  == null)
        {
            return;
        }
        switch(currentProgram) {
            case PROGRAM1:
                pfrag.setUpProgramNameText(FreqModelHolder.freqModel1.name);
                break;
            case PROGRAM2:
                pfrag.setUpProgramNameText(FreqModelHolder.freqModel2.name);
                break;
            case PROGRAM3:
                pfrag.setUpProgramNameText(FreqModelHolder.freqModel3.name);
                break;
            case PROGRAM4:
                pfrag.setUpProgramNameText(FreqModelHolder.freqModel4.name);
                break;
            case PROGRAM5:
                pfrag.setUpProgramNameText(FreqModelHolder.freqModel5.name);
                break;
            case PROGRAM6:
                pfrag.setUpProgramNameText(FreqModelHolder.freqModel6.name);
                break;
            case PROGRAM7:
                pfrag.setUpProgramNameText(FreqModelHolder.freqModel7.name);
                break;
            case PROGRAM8:
                pfrag.setUpProgramNameText(FreqModelHolder.freqModel8.name);
                break;
            case PROGRAM9:
                pfrag.setUpProgramNameText(FreqModelHolder.freqModel9.name);
                break;
            case PROGRAM10:
                pfrag.setUpProgramNameText(FreqModelHolder.freqModel10.name);
                break;
        }
    }

    @Override
    public void drawableState()
    {
        isRecoverConnection = false;
        if(recievedStringRecover.equals("12"))
        {
            pfrag.drawCircle(R.mipmap.circle1);
        }
        if(recievedStringRecover.equals("13"))
        {
            pfrag.drawCircle(R.mipmap.circle2);
        }
        if(recievedStringRecover.equals("14"))
        {
            pfrag.drawCircle(R.mipmap.circle3);
        }
        if(recievedStringRecover.equals("15"))
        {
            pfrag.drawCircle(R.mipmap.circle4);
        }
        recievedStringRecover = "";
    }

    private  void setSignalLevel(int butteryLevel, int signalLevel)
    {
        //---signal
        final int maxLevel = 850;
        final int minLevel = 132;

        if( signalLevel < minLevel)
        {
            signalLevel = minLevel;
        }

        if( signalLevel > maxLevel)
        {
            signalLevel = maxLevel;
        }

        final int finalSignalLevel = signalLevel;
        //---signal



        //---buttery
        final int maxButteryLevel = 970;
        final int minButteryLevel = 760;


        if (butteryLevel <= minButteryLevel)
        {
            butteryLevel = minButteryLevel;
        }

        if(butteryLevel >= minButteryLevel && butteryLevel <= minButteryLevel + 10)
        {
            Toast.makeText(getBaseContext(), "Low battery level. Connect the charger", Toast.LENGTH_SHORT).show();
        }


        else if (butteryLevel > maxButteryLevel)
        {
            butteryLevel = maxButteryLevel;
        }

        int diff = maxButteryLevel - minButteryLevel;

        final int pecent25 = (diff * 25) / 100 + minButteryLevel;
        final int pecent50 = (diff * 50) / 100 + minButteryLevel;
        final int pecent75 = (diff * 75) / 100 + minButteryLevel;


        final int finalButteryLevel = butteryLevel;
        //---buttery

        signalLineContainer.post(new Runnable()
        {
            @Override
            public void run()
            {
                int maxWdth =  signalLineContainer.getWidth();
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)signalImageView.getLayoutParams();

                int diff = maxLevel - minLevel;
                int sigDif = finalSignalLevel - minLevel;

                int res = sigDif * maxWdth / diff;
                layoutParams.width = res;

                signalImageView.setLayoutParams(layoutParams);

                if (finalButteryLevel >= minButteryLevel && finalButteryLevel <= pecent25)
                {
                    batteryImageView.setBackgroundResource(R.mipmap.battery1);
                }
                else if (finalButteryLevel > pecent25 && finalButteryLevel <= pecent50)
                {
                    batteryImageView.setBackgroundResource(R.mipmap.battery2);

                }
                else if (finalButteryLevel > pecent50 && finalButteryLevel <= pecent75)
                {
                    batteryImageView.setBackgroundResource(R.mipmap.battery3);

                }
                else if (finalButteryLevel > pecent75 && finalButteryLevel <= maxButteryLevel)
                {
                    batteryImageView.setBackgroundResource(R.mipmap.battery4);

                }

                TimerController.getInstance().startTimer();
            }
        });


    }

    private void setButteryLevel(int butteryLevel, int signalLevel)
    {
        final int maxLevel = 447;
        final int minLevel = 358;

        if (butteryLevel <= minLevel)
        {
            butteryLevel = minLevel;
        }

        else if (butteryLevel > maxLevel)
        {
            butteryLevel = maxLevel;
        }

        int diff = maxLevel - minLevel;

        final int pecent25 = (diff * 25) / 100 + minLevel;
        final int pecent50 = (diff * 50) / 100 + minLevel;
        final int pecent75 = (diff * 75) / 100 + minLevel;


        final int finalButteryLevel = butteryLevel;
        batteryImageView.post(new Runnable() {
            @Override
            public void run() {

                if (finalButteryLevel >= minLevel && finalButteryLevel <= pecent25)
                {
                    batteryImageView.setBackgroundResource(R.mipmap.battery1);

                }
                else if (finalButteryLevel > pecent25 && finalButteryLevel <= pecent50)
                {
                    batteryImageView.setBackgroundResource(R.mipmap.battery2);

                }
                else if (finalButteryLevel > pecent50 && finalButteryLevel <= pecent75)
                {
                    batteryImageView.setBackgroundResource(R.mipmap.battery3);

                }
                else if (finalButteryLevel > pecent75 && finalButteryLevel <= maxLevel)
                {
                    batteryImageView.setBackgroundResource(R.mipmap.battery4);

                }
            }
        });



    }

}
