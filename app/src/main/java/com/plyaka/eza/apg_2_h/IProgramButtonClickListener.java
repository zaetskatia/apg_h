package com.plyaka.eza.apg_2_h;

/**
 * Created by eza on 18.02.18.
 */

public interface IProgramButtonClickListener {
    void programButtonClick(EProgram program);
    void GoToMainFragment();
    void setProgName();
    void drawableState();

}
