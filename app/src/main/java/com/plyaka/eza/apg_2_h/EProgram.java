package com.plyaka.eza.apg_2_h;

/**
 * Created by eza on 18.02.18.
 */

public enum EProgram
{
    PROGRAM1,
    PROGRAM2,
    PROGRAM3,
    PROGRAM4,
    PROGRAM5,
    PROGRAM6,
    PROGRAM7,
    PROGRAM8,
    PROGRAM9,
    PROGRAM10
}
