package com.plyaka.eza.apg_2_h;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Fragment;

import android.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


public class ProgramFragment extends Fragment {
    Button btnStop;
    TextView infoTv;
    ImageView progressImageView;
    ProgressBar pbCircle;
    LayoutInflater inflater;
    View view;
    IProgramButtonClickListener programButtonClickListener;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try {
            programButtonClickListener = (IProgramButtonClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.inflater=inflater;
        view = this.inflater.inflate(R.layout.fragment_program, null);
        initElements();
        setActions();

        programButtonClickListener.setProgName();
        drawCircle(R.mipmap.circle);
        programButtonClickListener.drawableState();
        return view;
    }


    private void setActions()
    {
        btnStop.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Stop");
                alertDialog.setMessage("Are you sure to stop program?");
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog,int which)
                    {
                          Bluetooth.writeData((byte) 5);
                        programButtonClickListener.GoToMainFragment();
                    }
                });
                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });
    }

    private void initElements()
    {
        btnStop = (Button) view.findViewById(R.id.btnStop);
        infoTv = (TextView) view.findViewById(R.id.infoTv);
        progressImageView = (ImageView) view.findViewById(R.id.progressImageView);
        pbCircle = (ProgressBar) view.findViewById(R.id.pbCircle);
    }

    void setUpProgramNameText(String text)
    {
        infoTv.setText(text);
    }

    void drawCircle(int resource)
    {
        progressImageView.setBackgroundResource(resource);
    }

}

