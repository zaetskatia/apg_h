package com.plyaka.eza.apg_2_h;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

/**
 * Created by eza on 17.02.18.
 */

public final class Bluetooth {
    public static byte status;

    public static Handler hBlue;
    public static byte blue_ENABLE = 0;
    static final int RECIEVE_MESSAGE = 1;
    private static BluetoothAdapter btAdapter = null;
    private static BluetoothSocket btSocket = null;
    public Intent enableBtIntent;
    public static ConnectedThread mConnectedThread;

    private  final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");//"00001101-0000-1000-8000-00805F9B34FB"

    // MAC-����� Bluetooth ������
    //   <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>
    //*0*/private static String address = "0E:10:07:09:0A:0C";
    //*1*/ private static String address = "0E:10:1A:09:0F:32";
    //*2*/ private static String address = "0E:10:21:09:0A:0C";
    //*3*/ private static String address = "0E:10:2B:09:0A:0F";
    //*4*/ private static String address = "0E:10:19:09:0F:26";
    //*5*/  private static String address = "0D:15:30:27:0B:32";
    //*6*/ private  String address = "0E:10:0E:09:0F:37";
    //*7*/ private static String address = "0E:10:05:09:11:0E";
    //*8*/ private static String address = "0E:10:27:09:0A:10";
    //*9*/      private static String address = "20:16:05:30:83:60";//HC-05
    //*10*/      private static String address = "20:16:05:30:82:93";//HC-05
    //*11*/      private static String address = "20:16:05:30:82:92";//HC-05
    //*12*/      private static String address = "20:16:05:30:82:91";
    //*13*/      private static String address = "20:16:05:30:82:61";
    //15*/      private static String address = "20:17:03:14:17:82";
    //*14*/      private static String address = "20:17:03:14:33:57"; //APG_3_H
    //*15*/      private static String address = "AB:03:56:78:A0:33";
    //*16*/      private static String address = "AB:03:56:78:A0:25";
    //*17*/      private static String address = "AB:03:56:78:A0:2B";

    /*MedZoo*/      private static String address = "AB:03:56:78:A0:33";
    //*MedZoo1*/      private static String address = "AB:03:56:78:A0:25";
    //*MedZoo2*/      private static String address = "AB:03:56:78:A0:29";
    //*MedZoo3*/      private static String address = "AB:03:56:78:A0:13";
  //*MedZoo4*/      private static String address = "20:17:03:14:33:57";

    public static String msg_Data;


    public Bluetooth()
    {
        super();
        status = 0;

        btAdapter = BluetoothAdapter.getDefaultAdapter();
        enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        checkBTState();
    }

    private void checkBTState()
    {
        if(btAdapter==null) {
            //Fatal Error Bluetooth is not supported;
            blue_ENABLE = 0;
        }
        else {
            blue_ENABLE = 1;
            if (btAdapter.isEnabled()) {
                // Bluetooth is turned on
            } else {
                //Prompt user to turn on Bluetooth
                   // btAdapter.enable();
            }
            BluetoothDevice device = btAdapter.getRemoteDevice(address);
            try {
                btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e1) {
                blue_ENABLE = 0;
                e1.printStackTrace();
            }
            // Discovery is resource intensive.  Make sure it isn't going on
            // when you attempt to connect and pass your message.
            btAdapter.cancelDiscovery();
        }

        if 	(blue_ENABLE == 1)
        {
            try {
                btSocket.connect();
                mConnectedThread = new ConnectedThread(btSocket);
                //  Toast.makeText(getBaseContext(), "...The connection is established and ready to send data...", Toast.LENGTH_SHORT).show();
                blue_ENABLE = 2;
                mConnectedThread.start();
            } catch (IOException e) {
                try {
                    btSocket.close();
                    //    Toast.makeText(getBaseContext(), "...Connection is NOT ESTABLISHED - try to restart the application ", Toast.LENGTH_SHORT).show();
                    blue_ENABLE = 1;
                    status = 10;
                } catch (IOException e2) {

                }
            }

        }
    }

    public static void writeData(byte iData)
    {
        if(mConnectedThread!=null)
        {
            mConnectedThread.write_Data(iData);
        }
    }

//-----------------------------------------------------------------------------

    private static class ConnectedThread extends Thread
    {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket)
        {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try
            {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            }
            catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        @Override
        public void run()
        {
            int bytes = 1; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {

                    bytes = mmInStream.available();
                    byte[] buffer = new byte[bytes];        // buffer store for the stream
                    mmInStream.read(buffer);
                    //    msg_Data = Arrays.toString(buffer);
//                    if (bytes == 2)
//                    {
//                        hBlue.obtainMessage(RECIEVE_MESSAGE, bytes, 2, buffer).sendToTarget();
//                    }
                    if(bytes > 0)
                    {
                        hBlue.obtainMessage(RECIEVE_MESSAGE, bytes, 1, buffer).sendToTarget();
                    }

                }
                catch (IOException e)
                {
                    break;
                }
            }
        }

        public  void write_Data(byte iData)
        {
            try
            {
                mmOutStream.write(iData);
            }
            catch (IOException e)
            {
            }
        }

        public void cancel()
        {
            try
            {
                mmSocket.close();
            }
            catch (IOException e) { }
        }
    }
}
