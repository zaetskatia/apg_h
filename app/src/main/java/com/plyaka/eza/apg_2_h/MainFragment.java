package com.plyaka.eza.apg_2_h;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TabHost;


public class MainFragment extends Fragment {
    Button btnProgram1;
    Button btnProgram2;
    Button btnProgram3;
    Button btnProgram4;
    Button btnProgram5;
    Button btnProgram6;
    Button btnProgram7;
    Button btnProgram8;
    Button btnProgram9;
    Button btnProgram10;
    LayoutInflater inflater;
    View view;
    IProgramButtonClickListener programButtonClickListener;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            programButtonClickListener = (IProgramButtonClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.inflater=inflater;
        view = this.inflater.inflate(R.layout.fragment_main, null);
        initElements();
        setActions();


        return view;
    }

    private void initElements()
    {
        btnProgram1 = (Button)view.findViewById(R.id.program1);
        btnProgram2 = (Button)view.findViewById(R.id.program2);
        btnProgram3 = (Button) view.findViewById(R.id.program3);
        btnProgram4 = (Button) view.findViewById(R.id.program4);
        btnProgram5 = (Button) view.findViewById(R.id.program5);
        btnProgram6 = (Button) view.findViewById(R.id.program6);
        btnProgram7 = (Button) view.findViewById(R.id.program7);
        btnProgram8 = (Button) view.findViewById(R.id.program8);
        btnProgram9 = (Button) view.findViewById(R.id.program9);
        btnProgram10 = (Button) view.findViewById(R.id.program10);

        btnProgram1.setText(FreqModelHolder.freqModel1.name);
        btnProgram2.setText(FreqModelHolder.freqModel2.name);
        btnProgram3.setText(FreqModelHolder.freqModel3.name);
        btnProgram4.setText(FreqModelHolder.freqModel4.name);
        btnProgram5.setText(FreqModelHolder.freqModel5.name);
        btnProgram6.setText(FreqModelHolder.freqModel6.name);
        btnProgram7.setText(FreqModelHolder.freqModel7.name);
        btnProgram8.setText(FreqModelHolder.freqModel8.name);
        btnProgram9.setText(FreqModelHolder.freqModel9.name);
        btnProgram10.setText(FreqModelHolder.freqModel10.name);
    }

    private void setActions()
    {
        btnProgram1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                programButtonClickListener.programButtonClick(EProgram.PROGRAM1);
            }
        });
        btnProgram2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                programButtonClickListener.programButtonClick(EProgram.PROGRAM2);
            }
        });
        btnProgram3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                programButtonClickListener.programButtonClick(EProgram.PROGRAM3);
            }
        });
        btnProgram4.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                programButtonClickListener.programButtonClick(EProgram.PROGRAM4);
            }
        });
        btnProgram5.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                programButtonClickListener.programButtonClick(EProgram.PROGRAM5);
            }
        });
        btnProgram6.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                programButtonClickListener.programButtonClick(EProgram.PROGRAM6);
            }
        });
        btnProgram7.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                programButtonClickListener.programButtonClick(EProgram.PROGRAM7);
            }
        });
        btnProgram8.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                programButtonClickListener.programButtonClick(EProgram.PROGRAM8);
            }
        });
        btnProgram9.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                programButtonClickListener.programButtonClick(EProgram.PROGRAM9);
            }
        });

        btnProgram10.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                programButtonClickListener.programButtonClick(EProgram.PROGRAM10);
            }
        });


    }

    public void qwe(String s)
    {
        btnProgram1.setText(s);
    }
}

