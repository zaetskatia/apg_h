package com.plyaka.eza.apg_2_h;

/**
 * Created by eza on 18.02.18.
 */

public final class FreqModelHolder {
    public static FreqModel freqModel1 = new FreqModel(7, 183, 6,73, 1, 250, "Program 1");
    public static FreqModel freqModel2= new FreqModel(7, 183, 6, 52, 6, 140, "Program 2");
    public static FreqModel freqModel3= new FreqModel(5, 252, 6, 140, 5, 47, "Program 3");

    public static FreqModel freqModel4= new FreqModel(7, 183, 6, 140, 1, 250, "Program 4");
    public static FreqModel freqModel5= new FreqModel(6, 52, 6, 140, 7, 183, "Program 5");

    public static FreqModel freqModel6= new FreqModel(6, 140, 104, 228, 7, 183, "Program 6");
    public static FreqModel freqModel7= new FreqModel(11, 230, 7, 52, 4, 69, "Program 7");
    public static FreqModel freqModel8= new FreqModel(6, 87, 6, 140, 1, 250, "Program 8");
    public static FreqModel freqModel9= new FreqModel(6, 52, 7, 183, 6, 92, "Program 9");
    public static FreqModel freqModel10= new FreqModel(2, 64, 15, 170, 7, 176, "Program 10");

    public static byte[] fraqArray1 = new byte[] {FreqModelHolder.freqModel1.Hi1, FreqModelHolder.freqModel1.Lo1,
            FreqModelHolder.freqModel1.Hi2,FreqModelHolder.freqModel1.Lo2,
            FreqModelHolder.freqModel1.Hi3,FreqModelHolder.freqModel1.Lo3};

    public static byte[] fraqArray2 = new byte[] {FreqModelHolder.freqModel2.Hi1, FreqModelHolder.freqModel2.Lo1,
            FreqModelHolder.freqModel2.Hi2,FreqModelHolder.freqModel2.Lo2,
            FreqModelHolder.freqModel2.Hi3,FreqModelHolder.freqModel2.Lo3};

    public static byte[] fraqArray3 = new byte[] {FreqModelHolder.freqModel3.Hi1, FreqModelHolder.freqModel3.Lo1,
            FreqModelHolder.freqModel3.Hi2,FreqModelHolder.freqModel3.Lo2,
            FreqModelHolder.freqModel3.Hi3,FreqModelHolder.freqModel3.Lo3};

    public static byte[] fraqArray4 = new byte[] {FreqModelHolder.freqModel4.Hi1, FreqModelHolder.freqModel4.Lo1,
            FreqModelHolder.freqModel4.Hi2,FreqModelHolder.freqModel4.Lo2,
            FreqModelHolder.freqModel4.Hi3,FreqModelHolder.freqModel4.Lo3};


    public static byte[] fraqArray5 = new byte[] {FreqModelHolder.freqModel5.Hi1, FreqModelHolder.freqModel5.Lo1,
            FreqModelHolder.freqModel5.Hi2,FreqModelHolder.freqModel5.Lo2,
            FreqModelHolder.freqModel5.Hi3,FreqModelHolder.freqModel5.Lo3};


    public static byte[] fraqArray6 = new byte[] {FreqModelHolder.freqModel6.Hi1, FreqModelHolder.freqModel6.Lo1,
            FreqModelHolder.freqModel6.Hi2,FreqModelHolder.freqModel6.Lo2,
            FreqModelHolder.freqModel6.Hi3,FreqModelHolder.freqModel6.Lo3};


    public static byte[] fraqArray7 = new byte[] {FreqModelHolder.freqModel7.Hi1, FreqModelHolder.freqModel7.Lo1,
            FreqModelHolder.freqModel7.Hi2,FreqModelHolder.freqModel7.Lo2,
            FreqModelHolder.freqModel7.Hi3,FreqModelHolder.freqModel7.Lo3};


    public static byte[] fraqArray8 = new byte[] {FreqModelHolder.freqModel8.Hi1, FreqModelHolder.freqModel8.Lo1,
            FreqModelHolder.freqModel8.Hi2,FreqModelHolder.freqModel8.Lo2,
            FreqModelHolder.freqModel8.Hi3,FreqModelHolder.freqModel8.Lo3};


    public static byte[] fraqArray9 = new byte[] {FreqModelHolder.freqModel9.Hi1, FreqModelHolder.freqModel9.Lo1,
            FreqModelHolder.freqModel9.Hi2,FreqModelHolder.freqModel9.Lo2,
            FreqModelHolder.freqModel9.Hi3,FreqModelHolder.freqModel9.Lo3};

    public static byte[] fraqArray10 = new byte[] {FreqModelHolder.freqModel10.Hi1, FreqModelHolder.freqModel10.Lo1,
            FreqModelHolder.freqModel10.Hi2,FreqModelHolder.freqModel10.Lo2,
            FreqModelHolder.freqModel10.Hi3,FreqModelHolder.freqModel10.Lo3};


}
